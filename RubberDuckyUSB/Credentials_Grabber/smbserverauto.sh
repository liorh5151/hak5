#!/usr/bin/bash

# This is a script for configuring SMB server (for Duckyscript).

echo "Installing some packages..."
sudo apt install python 
# By default Python version will be 2.7.
sudo apt install pip
sudo apt install net-tools
sudo apt install git
git clone https://github.com/SecureAuthCorp/impacket.git
echo "Success with file cloning..."
cd impacker
sudo apt-get install python-setuptools
sudo python setup.py install
sudo pip install -r requirement.txt
sudo pip install -r requirement-test.txt
echo "Done with installation..."
cd examples
# making sure ./smbserver script is working --> hostname:~$./smbserver
# ./smbserver {ShareName} {SharePath}
echo "Working on it..."
sudo ./smbserver tmp /tmp/
echo "It's working! Our server is up and running..."

# The IP address we'll need to write on the ducky script will be stored in a variable called 'IP'.
# $IP=(hostname -I | awk '{print $1}')
# $IP